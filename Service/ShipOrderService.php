<?php
namespace Conneqt\ShipmentCreator\Service;

use Conneqt\ShipmentCreator\Api\Data\ConneqtShipmentInterface;
use Exception;
use Magento\Framework\Exception\LocalizedException;
use Magento\Sales\Api\Data\ShipmentInterface;
use Magento\Sales\Api\Data\ShipmentItemCreationInterfaceFactory;
use Magento\Sales\Api\ShipOrderInterface;
use Magento\Sales\Model\Order;
use Magento\Sales\Model\OrderFactory;

class ShipOrderService
{
    private OrderFactory $orderFactory;
    private ShipOrderInterface $shipOrder;
    private ShipmentItemCreationInterfaceFactory $shipmentItemFactory;
    private ShipmentInterface $shipmentFactory;

    public function __construct(
        OrderFactory $orderFactory,
        ShipOrderInterface $shipOrder,
        ShipmentItemCreationInterfaceFactory $shipmentItemFactory,
        ShipmentInterface $shipmentFactory
    ) {
        $this->orderFactory = $orderFactory;
        $this->shipOrder = $shipOrder;
        $this->shipmentItemFactory = $shipmentItemFactory;
        $this->shipmentFactory = $shipmentFactory;
    }

    /**
     * @throws Exception
     */
    public function shipOrder(ConneqtShipmentInterface $conneqtShipment, $existingOrder = null): array
    {
        $incrementId = $conneqtShipment->getIncrementId();
        $shipmentItems = $conneqtShipment->getShipmentItems();
        /** @var Order $order */
        $order = $existingOrder ?? $this->orderFactory->create()->loadByIncrementId($incrementId);
        $shipment = $this->shipmentFactory;
        $itemsToShip = [];

        foreach ($order->getAllItems() as $orderItem) {
            $productSku = $orderItem->getSku();
            $key = array_search($productSku, array_column($shipmentItems, 'sku'));
            if ($key === false) {
                continue;
            }
            $shipQty = (isset($shipmentItems[$key]['qty'])) ? $shipmentItems[$key]['qty'] : 0;

            if ($shipQty !== 0 && !$orderItem->getQtyToShip() || $orderItem->getIsVirtual()) {
                continue;
            }

            $maxQtyToShip = $orderItem->getQtyToShip();

            if ($maxQtyToShip > 0){
                $shipmentItem = $this->shipmentItemFactory->create();
                $shipmentItem->setOrderItemId($orderItem->getItemId())->setQty($maxQtyToShip);

                $itemsToShip[] = $shipmentItem;
            }

        }

        $shipment->setItems($itemsToShip);

        if(count($shipment->getItems()) > 0 ){
            $_items = $shipment->getItems();
            $shipmentId = $this->shipOrder->execute(
                $order->getEntityId(),
                $_items
            );
            return [['success' => true, 'shipment_id' => $shipmentId]];
        }

        return [['success' => true, 'message' => 'No items to ship']];

    }

}
