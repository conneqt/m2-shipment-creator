<?php
namespace Conneqt\ShipmentCreator\Service;

use Exception;
use Magento\Sales\Api\Data\ShipmentInterface;
use Magento\Sales\Api\Data\ShipmentItemCreationInterfaceFactory;
use Magento\Sales\Api\OrderStatusHistoryRepositoryInterface;
use Magento\Sales\Api\ShipOrderInterface;
use Magento\Sales\Model\Order;
use Magento\Sales\Model\OrderFactory;
use Magento\Sales\Model\Service\InvoiceService;
use Magento\Framework\DB\Transaction;

class MarkOrderCompleted
{
    private OrderFactory $orderFactory;
    private ShipOrderInterface $shipOrder;
    private ShipmentItemCreationInterfaceFactory $shipmentItemFactory;
    private ShipmentInterface $shipmentFactory;
    private OrderStatusHistoryRepositoryInterface $orderStatusRepository;
    private InvoiceService $invoiceService;
    private Transaction $transaction;

    public function __construct(
        OrderFactory $orderFactory,
        ShipOrderInterface $shipOrder,
        ShipmentItemCreationInterfaceFactory $shipmentItemFactory,
        ShipmentInterface $shipmentFactory,
        OrderStatusHistoryRepositoryInterface $orderStatusRepository,
        InvoiceService $invoiceService,
        Transaction $transaction

    ) {
        $this->orderFactory = $orderFactory;
        $this->shipOrder = $shipOrder;
        $this->shipmentItemFactory = $shipmentItemFactory;
        $this->shipmentFactory = $shipmentFactory;
        $this->orderStatusRepository = $orderStatusRepository;
        $this->invoiceService = $invoiceService;
        $this->transaction = $transaction;
    }

    /**
     * @throws Exception
     */
    public function checkAndMarkOrderAsComplete(string $incrementId): bool
    {
        $order = $this->orderFactory->create()->loadByIncrementId($incrementId);

        $hasInvoices = (count($order->getInvoiceCollection()->getAllIds()) >= 1) ?? false;
        if ($hasInvoices) {
            return false;
        }

        $isBankTransferPayment = ($order->getPayment()->getMethod() == 'banktransfer' ) ?? false;
        if (!$isBankTransferPayment) {
            return false;
        }

        $totalQtyToShip = 0;
        foreach ($order->getAllItems() as $orderItem) {
            $totalQtyToShip += $orderItem->getQtyToShip();
        }

        if ($totalQtyToShip > 0) {
            return false;
        }

        if ($order->canInvoice()) {
            $invoice = $this->invoiceService->prepareInvoice($order);
            $invoice->register();
            $invoice->save();

            $transactionSave =
                $this->transaction
                    ->addObject($invoice)
                    ->addObject($invoice->getOrder());
            $transactionSave->save();

             $comment = $order->addCommentToStatusHistory(
                 __('Created Magento invoice automatically: #%1 Start setting to complete.', $invoice->getId())
             );
            $this->orderStatusRepository->save($comment);
        }

        $orderState = Order::STATE_COMPLETE;
        $order
            ->setState($orderState)
            ->setStatus($orderState);
        $order->save();

        $comment = $order->addCommentToStatusHistory(
            'Order set to complete automatically.'
        );
        $this->orderStatusRepository->save($comment);

        return true;

    }

}
