<?php

namespace Conneqt\ShipmentCreator\Api\Data;

interface ConneqtShipmentInterface
{
    /**
     * String constants for property names
     */
    const INCREMENT_ID = 'increment_id';
    const SHIPMENT_ITEMS = 'shipment_items';

    /**
     * Getter for IncrementId.
     *
     * @return int|null
     */
    public function getIncrementId(): ?int;

    /**
     * Setter for IncrementId.
     *
     * @param int|null $incrementId
     *
     * @return void
     */
    public function setIncrementId(?int $incrementId): void;

    /**
     * Getter for ShipmentItems.
     *
     * @return array|null
     */
    public function getShipmentItems(): ?array;

    /**
     * Setter for ShipmentItems.
     *
     * @param array|null $shipmentItems
     *
     * @return void
     */
    public function setShipmentItems(?array $shipmentItems): void;
}
