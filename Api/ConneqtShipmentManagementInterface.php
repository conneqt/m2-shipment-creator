<?php

namespace Conneqt\ShipmentCreator\Api;

use Magento\Sales\Api\Data\ShipmentInterface;

interface ConneqtShipmentManagementInterface
{
    /**
     * Setter for Ship.
     * @return array
     */
    public function execute();
}
