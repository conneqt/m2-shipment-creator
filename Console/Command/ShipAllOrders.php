<?php

namespace Conneqt\ShipmentCreator\Console\Command;

use Conneqt\ShipmentCreator\Api\Data\ConneqtShipmentInterfaceFactory;
use Conneqt\ShipmentCreator\Service\ShipOrderService;
use DateTime;
use Magento\Sales\Api\Data\OrderInterface;
use Magento\Sales\Model\ResourceModel\Order\CollectionFactory;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ShipAllOrders extends Command
{


    private ConneqtShipmentInterfaceFactory $conneqtShipmentFactory;
    private CollectionFactory $orderCollectionFactory;
    private \Magento\Framework\Stdlib\DateTime\DateTime $date;
    private \Magento\Framework\App\State $state;
    private \Magento\Framework\ObjectManagerInterface $objectmanager;

    public function __construct(
        ConneqtShipmentInterfaceFactory $conneqtShipmentFactory,
        \Magento\Framework\ObjectManagerInterface $objectmanager,
        CollectionFactory $orderCollectionFactory,
        \Magento\Framework\Stdlib\DateTime\DateTime $date,
        \Magento\Framework\App\State $state,
        string $name = null
    ) {
        parent::__construct($name);
        $this->conneqtShipmentFactory = $conneqtShipmentFactory;
        $this->orderCollectionFactory = $orderCollectionFactory;
        $this->date = $date;
        $this->state = $state;
        $this->objectmanager = $objectmanager;
    }

    /**
     * Initialization of the command.
     */
    protected function configure()
    {
        $this->setName('epartment:orders:ship');
        $this->setDescription('Ship all orders');
        parent::configure();
    }

    /**
     * CLI command description.
     *
     * @param InputInterface $input
     * @param OutputInterface $output
     *
     * @return void
     */
    protected function execute(InputInterface $input, OutputInterface $output): void
    {
        $this->state->setAreaCode(\Magento\Framework\App\Area::AREA_ADMINHTML);
        $collection = $this->orderCollectionFactory->create();
        $shipService = $this->objectmanager->get(\Conneqt\ShipmentCreator\Service\ShipOrderService::class);

        $date = $this->date->date('Y-m-d H:i:s'); // current date
        $weekAgo = $this->date->date('Y-m-d H:i:s', strtotime($date." -1 month")); //next day date//next day date
        $week2Ago = $this->date->date('2017-03-01 00:00:00'); //next day date//next day date

        $collection->addFieldToSelect('*')
            ->addFieldToFilter('status',
                ['in' => ['processing']]
            )->addFieldToFilter('created_at',
                ['lteq' => $weekAgo]
            )->addFieldToFilter('created_at',
                ['gteq' => $week2Ago]
            )->setOrder(
                'created_at',
                'desc'
            );

        $collection->load();

        $progressBar = new \Symfony\Component\Console\Helper\ProgressBar($output, $collection->count());
        $progressBar->setFormat('%bar% %current%/%max% [Memory Usage: %memory%]');
        $progressBar->start();

        /** @var OrderInterface $order */
        foreach ($collection->getItems() as $order){
            $grandTotal = $order->getGrandTotal();
            $totalInvoiced = $order->getTotalInvoiced();

            if ($grandTotal !== $totalInvoiced) {
                continue;
            }

            $conneqtShipment = $this->conneqtShipmentFactory->create();
            $incrementId = $order->getIncrementId();
            $conneqtShipment->setIncrementId($incrementId);
            $items = [];
            foreach ($order->getItems() as $orderItem) {
                $items[] = [
                    'sku' => $orderItem->getSku(),
                    'qty' => (int) $orderItem->getQtyOrdered()
                ];
            }
            $conneqtShipment->setShipmentItems($items);

            try {
                $results = $shipService->shipOrder($conneqtShipment, $order);
            } catch (\Magento\Framework\Exception\LocalizedException|\Exception $e) {
                $results = $e->getMessage();
            }

            $results = json_encode($results);
            $progressBar->advance();

            $output->writeln($results);
        }

        $progressBar->finish();
    }
}
