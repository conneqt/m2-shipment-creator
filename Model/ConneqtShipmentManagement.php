<?php

namespace Conneqt\ShipmentCreator\Model;

use Conneqt\ShipmentCreator\Api\ConneqtShipmentManagementInterface;
use Conneqt\ShipmentCreator\Api\Data\ConneqtShipmentInterfaceFactory;
use Conneqt\ShipmentCreator\Service\ShipOrderService;
use Magento\Framework\DataObject;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Serialize\SerializerInterface;
use Magento\Framework\Webapi\Rest\Request;
use Magento\Sales\Api\Data\ShipmentInterface;

class ConneqtShipmentManagement extends DataObject implements ConneqtShipmentManagementInterface
{
    /**
     * @var ConneqtShipmentInterfaceFactory
     */
    private $shipmentFactory;
    /**
     * @var SerializerInterface
     */
    private $jsonSerializer;
    /**
     * @var RequestInterface|Request
     */
    private $request;
    /**
     * @var ShipOrderService
     */
    private $shipOrderService;

    protected $postData;

    /**
     * @param ConneqtShipmentInterfaceFactory $shipmentFactory
     * @param RequestInterface $request
     * @param array $data
     */
    public function __construct(
        ConneqtShipmentInterfaceFactory $shipmentFactory,
        SerializerInterface $jsonSerializer,
        Request $request,
        ShipOrderService $shipOrderService,
        array $data = []
    ) {
        parent::__construct($data);
        $this->shipmentFactory = $shipmentFactory;
        $this->jsonSerializer = $jsonSerializer;
        $this->request = $request;
        $this->shipOrderService = $shipOrderService;
    }

    /**
     * @inheritDoc
     * @throws LocalizedException
     */
    public function execute(): array
    {
        $this->postData = $this->request->getBodyParams();
        $this->validate();

        $conneqtShipment = $this->shipmentFactory->create();
        $conneqtShipment->addData($this->postData);

        return $this->shipOrderService->shipOrder($conneqtShipment);
    }


    /**
     * @throws LocalizedException
     */
    private function validate()
    {
        $this->fieldExists($this->postData, 'increment_id');
        $this->fieldExists($this->postData, 'shipment_items');

        if (!is_array($this->postData['shipment_items'])) {
            throw new LocalizedException(__('The field %s is not an array', 'items'));
        }

        foreach ($this->postData['shipment_items'] as $item){
            $this->fieldExists($item, 'sku', 'The field %s is missing for one of the items');
            $this->fieldExists($item, 'qty', 'The field %s is missing for one of the items');
        }
    }

    /**
     * @throws LocalizedException
     */
    private function fieldExists(array $data, $field, $customMessage = null): void
    {
        if (key_exists($field, $data) && !empty($data[$field])) {
            return;
        }

        if (!empty($customMessage)) {
            throw new LocalizedException(__($customMessage, $field));
        }
        throw new LocalizedException(__('The field %s is missing', $field));
    }
}
