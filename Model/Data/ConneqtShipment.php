<?php

namespace Conneqt\ShipmentCreator\Model\Data;

use Conneqt\ShipmentCreator\Api\Data\ConneqtShipmentInterface;
use Magento\Framework\DataObject;

class ConneqtShipment extends DataObject implements ConneqtShipmentInterface
{
    /**
     * @inheritDoc
     */
    public function getIncrementId(): ?int
    {
        return $this->getData(self::INCREMENT_ID) === null ? null
            : (int)$this->getData(self::INCREMENT_ID);
    }

    /**
     * @inheritDoc
     */
    public function setIncrementId(?int $incrementId): void
    {
        $this->setData(self::INCREMENT_ID, $incrementId);
    }

    /**
     * @inheritDoc
     */
    public function getShipmentItems(): ?array
    {
        return $this->getData(self::SHIPMENT_ITEMS);
    }

    /**
     * @inheritDoc
     */
    public function setShipmentItems(?array $shipmentItems): void
    {
        $this->setData(self::SHIPMENT_ITEMS, $shipmentItems);
    }
}
